import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs"
import { PersonDTO } from "../person/requestDto/personDto";
import { catchError } from 'rxjs/operators';
import { endPoints } from '../constant/end_points';

@Injectable({
  providedIn: 'root'
})
export class RestService {

  constructor(private http: HttpClient) { }
  
  postApiRest(personDto: PersonDTO): Observable<object>{

    return this.http.post(endPoints.birthday_logic_rest, personDto);

  }

}	