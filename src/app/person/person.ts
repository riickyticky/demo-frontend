  
export class Person{

  personName: string;
  personLastname: string;
  dateOfBirthday: Date;
  personAge: number;
  birthdayCongratulations: string;
  remainingDays: string;

}