import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Person } from '../person/person';
import { Input, OnChanges, SimpleChanges } from '@angular/core';
import { RestService } from '../helpers/rest.service';
import { NgxSpinnerService } from "ngx-spinner";
import { PersonDTO } from "../person/requestDto/personDto";
import { catchError } from 'rxjs/operators';
import { Observable } from "rxjs"

@Component({
  selector: 'app-person',
  templateUrl: './person.component.html',
  styleUrls: ['./person.component.css']
})
export class PersonComponent implements OnInit {

  responsePerson: Person = new Person;
  personDto: PersonDTO = new PersonDTO;
  isBirthday: boolean = false;
  afterCall: boolean = false;
  h1Bool: boolean = true;
  error: boolean = false;
  placeholder: string = "Fabian Jorge Espinoza Cardenas";
  errorName: string;
  internalServerError: boolean = false;

  constructor(private restService: RestService, private ngxSpinnerService: NgxSpinnerService ) { }

  ngOnInit(): void {}

  sendForm(){
      this.postServiceRest();
  }

  cleanForm(){
    this.isBirthday = false;
    this.afterCall = false;
    this.h1Bool = true;
    this.error = false;
    this.internalServerError=false;
    this.personDto = new PersonDTO;
  }

  isDefined(value) { return typeof value !== 'undefined'; }

  postServiceRest(){

    this.ngxSpinnerService.show();

    this.restService.postApiRest(this.personDto)
        .subscribe(
          (data:any) => {
          	this.afterCall = true;
            this.h1Bool = false;
          	this.responsePerson = data;
            if(this.isDefined(this.responsePerson.birthdayCongratulations)){
            	this.isBirthday = true;
            }
            if(this.isDefined(data.error)){
            	this.error=true;
            	this.afterCall = false;
            	this.errorName = data.error;
            }
            
            this.ngxSpinnerService.hide();
          },
            err => {
              this.error=true;
              this.internalServerError=true;
              this.ngxSpinnerService.hide();
	          console.log('Error interno server', err);
	        }
        );

    this.cleanForm();
  }


}
